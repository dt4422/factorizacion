package step00;
public class Rental {

	Movie _movie;
	private int _daysRented;

	public Rental(Movie movie, int daysRented) {
		_movie = movie;
		_daysRented = daysRented;
	}

	public int getDaysRented() {
		return _daysRented;
	}

	public Movie getMovie() {
		return _movie;
	}

	/**
	 * @deprecated Use {@link step00.Movie#getCharge(step00.Rental)} instead
	 */
	double getCharge() {
		return _movie.getCharge(this);
	}

	/**
	 * @deprecated Use {@link step00.Movie#getFrequentRenterPoints(step00.Rental)} instead
	 */
	int getFrequentRenterPoints() {
		return _movie.getFrequentRenterPoints(this);
	}
	
}

