package step00;
public class Movie {

	public static final int CHILDRENS = 2;
	public static final int REGULAR = 0;
	public static final int NEW_RELEASE = 1;

	private String _title;
	private int _priceCode;
	
	public Movie(String title, int priceCode) {
		_title = title;
		_priceCode = priceCode;
	}

	public int getPriceCode() {
		return _priceCode;
	}

	public void setPriceCode(int arg) {
		_priceCode = arg;
	}

	public String getTitle() {
		return _title;
	}

	

	double getCharge(int ndays) {
		
		double result = 0;
		switch (getPriceCode()) {
			case Movie.REGULAR:
				result += 2;
				if (ndays > 2)
					result += (ndays - 2) * 1.5;
				break;
			case Movie.NEW_RELEASE:
				result += ndays * 3;
				break;
			case Movie.CHILDRENS:
				result += 1.5;
				if (ndays > 3)
					result += (ndays - 3) * 1.5;
				break;
		}
		return result;
	}

	int getFrequentRenterPoints(int ndays) {
		// add frequent renter points
		int result = 1;
		// add bonus for a two day new release rental
		if ((getPriceCode() == Movie.NEW_RELEASE) &&
				ndays > 1) result ++;
		return result;
	}
}

